<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //index method - returns welcome.blade.php with all products
    public function welcome () {
    	$products = Product::all();
    	return view('welcome')->with('products', $products);
    }

    public function cart () {

    }



}

