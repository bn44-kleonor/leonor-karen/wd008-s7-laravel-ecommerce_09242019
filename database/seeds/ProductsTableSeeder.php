<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert
        ([
        	[   'name' => 'broccoli', 
                'price' => 10, 
                'description' => 'description of brocolli', 
                'stock' => 100, 
                'category_id' => 1
            ],
        	[   'name' => 'beef', 
                'price' => 20, 
                'description' => 'description of beef', 
                'stock' => 200, 
                'category_id' => 2
            ],
        	[   'name' => 'salmon', 
                'price' => 30, 
                'description' => 'description of salmon', 
                'stock' => 300, 
                'category_id' => 3
            ],
        	[   'name' => 'mango', 
                'price' => 40, 
                'description' => 'description of mango', 
                'stock' => 400, 
                'category_id' => 4
            ]
        ]);
    }
}
