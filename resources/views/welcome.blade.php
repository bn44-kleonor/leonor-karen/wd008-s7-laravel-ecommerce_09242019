@extends("layouts.app")
@section("content")
{{-- dd($products) --}}
    <!-- PRODUCTS IN CARDS -->
    
    <div class="row">
    	@foreach($products as $product)
    	<div class="col">
		  <div class="card">
		    <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
		    <div class="card-body">
		      <h5 class="card-title">{{ $product->name }}</h5>
		      <p class="card-text">{{ $product->description }}.</p>
		      <p class="card-text">
		      	$ {{ $product->price }}
		      </p>
		    </div>
		  </div>
		  <button type="button" class="btn btn-primary">
		    	Add to Cart
		  </button>
		</div>
		@endforeach
    </div>    
@endsection
