@extends("layouts.app")
@section("content")
	<!-- PRODUCTS IN TABLE -->
	<div class="row">
		<div class="col">
			<?php $i = 1; ?>
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col" width="25%">#</th>
			      <th scope="col" width="25%">Name</th>
			      <th scope="col" width="25%">Stock</th>
			      <th scope="col" width="25%">Price</th>
			      <th scope="col" width="25%">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach($products as $product)
			    <tr>
			      <th scope="row"><?php echo $i++; ?></th>
			      <td>{{ $product->name }}</td>
			      <td>{{ $product->stock }}</td>
			      <td>$ {{ $product->price }}</td>
		          <td>
		            <div class="d-flex flex-row">
		                <!-- READ -->
		                <a href="#" class="btn btn-primary mr-1">	
		                	View
		                </a>
		                <!-- UPDATE -->
		                <a href="#" class="btn btn-warning mr-1">
		                	Edit
		                </a>
		                <!-- DELETE -->
		                <a href="#" class="btn btn-danger mr-1">
		                	Delete
		                </a>
		            </div>
		          </td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
	</div>
	<div class="row">
        <div class="col">
            <a href="#" class="btn btn-success">Add Product</a>
        </div>
    </div>
@endsection